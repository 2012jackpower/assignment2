const actors = [
    {
    "adult": false,
    "biography": "John David Washington (born July 28, 1984) is an American actor and former American football running back. He shifted to an acting career like his father, Denzel Washington and mother, Pauletta Washington. In 2015, he started with the HBO comedy series Ballers and his role in BlacKkKlansman (2018) brought him a Golden Globe nomination for Best Actor. He also starred in Tenet (2020) by Christopher Nolan, and Malcolm & Marie (2021).",
    "birthday": "1984-07-28",
    "id": 1117313,
    "imdb_id": "nm0913475",
    "name": "John David Washington",
    "place_of_birth": "Los Angeles, California, USA",
    "profile_path": "/eWNCcG4DqqhFKtWP56Ds8MiKPXB.jpg",
    "known_for": [
        {
        "movie_id": 577922
        },
        {
        "movie_id": 487558
        },
        {
        "movie_id": 429203
        }
        ]
    },
    {
    "adult": false,
    "biography" : "Robert Douglas Thomas Pattinson (born May 13, 1986) is an English actor from Barnes, London, UK. After his father convinced him to join the local theatre club at the age of 15 because he was quite shy, he began his film career by playing Cedric Diggory in Harry Potter and the Goblet of Fire (2005) when he was 18. At 22, the role of vampire Edward Cullen in The Twilight Saga films (2008-12) brought him to worldwide stardom.\n\nHe received critical acclaim for his raw and transformative performances by starring in independent films from auteur directors, especially in Good Time (2017) and The Lighthouse (2019), with critics calling them a revelation and career-defining. Other films are Cosmopolis (2012), The Rover (2014), The Lost City of Z (2016), High Life (2018), The King (2019) and The Devil All the Time (2020). He returned to big-budget films by starring in Tenet (2020) by Christopher Nolan, and by playing Batman in DC's The Batman (2022), directed by Matt Reeves.",
    "birthday": "1986-05-13",
    "id": 11288,
    "imdb_id": "nm1500155",
    "name": "Robert Pattinson",
    "place_of_birth": "Barnes, London, England, UK",
    "profile_path": "/8A4PS5iG7GWEAVFftyqMZKl3qcr.jpg",
    "known_for": [
        {
        "movie_id": 577922
        }
        ]
    }
]
export default actors;