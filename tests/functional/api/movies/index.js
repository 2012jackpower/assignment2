// import axios from "axios";
// import chai from "chai";
// import { application } from "express";
// import request from "supertest";
// const mongoose = require("mongoose");
// import Movie from "../../../../api/movies/movieModel";
// import api from "../../../../index";
// import movies from "../../../../seedData/movies";
// const fetch = require("node-fetch")
// //import { getAuth } from "firebase/auth";

// const expect = chai.expect;
// let db;
// var token;
// var authToken;

// const admin = require('firebase-admin')
// var serviceAccount = require("../../../../reactadminservice.json")


// // if(!admin.apps.length){
// //   admin.initializeApp({
// //       credential: admin.credential.cert(serviceAccount),
// //     });
// //   }else{
// //       admin.app()
// //   }

//   // const token1 = admin.auth().createCustomToken("xKdXw06u2tUVE72ETrJVwevCNQo1")
//   // const token2 = await token1
//   // console.log(token1,token2)

// // async function firebaseSetupAuth(){
// //   const token1 = admin.auth().createCustomToken("xKdXw06u2tUVE72ETrJVwevCNQo1")
// //   const token2 = await token1
// //   token = token2
// //   const url = "https://identitytoolkit.googleapis.com/v1/accounts:signInWithCustomToken?key=AIzaSyBoKaBKWe9x2LS_n5AHCIEM-5AB_dC99mg"
// //   const fetchBody = {"token": token,"returnSecureToken":true }
// //   //const ax = await 
// //   axiosFetch(url, fetchBody);
//   //console.log(ax)
//   // console.log(JSON.stringify(fetchBody))
//   // const response1 = await fetch(url,{
//   //   method: "post",
//   //   body: JSON.stringify(fetchBody),
//   //   headers: {'Content-Type': 'application/json'}
//   // })
//   // const response2 = await response1.json();
//   // authToken = response2.idToken;
// }

// // async function axiosFetch(url,body){
// //   const res =  axios.post(url,body)
// //   const res1 = await res;
// //   console.log(res1)
// //   //return res1
// // }

// describe("Movies endpoint", () => {
//   before(() => {
//     //firebaseSetupAuth();

//   // admin.auth()
//   // .createCustomToken("xKdXw06u2tUVE72ETrJVwevCNQo1")
//   // .then((customToken) => {
//   //   getTestToken(customToken)
//   //   console.log(customToken)
//   // })
//   // .catch((error) => {
//   //   console.log('Error creating custom token:', error);
//   // });
//     // mongoose.connect(process.env.MONGO_DB, {
//     //   useNewUrlParser: true,
//     //   useUnifiedTopology: true,
//     // });
//     // db = mongoose.connection;
//     // request(api)
//     // .post("/api/users?action=authenticate")
//     // .send({
//     //   username: "user1",
//     //   password: "test1",
//     // })
//     // .expect(200)
//     // .then((res) => {
//     //   expect(res.body.success).to.be.true;
//     //   expect(res.body.token).to.not.be.undefined;
//     //   token = res.body.token.substring(7);
//     //   });
//   });

//   after(async () => {
//     try {
//       await db.dropDatabase();
//     } catch (error) {
//       console.log(error);
//     }
//   });

//   beforeEach(async () => {
//     try {
//       await Movie.deleteMany();
//       await Movie.collection.insertMany(movies);
//     } catch (err) {
//       console.error(`failed to Load user Data: ${err}`);
//     }
//   });
//   afterEach(() => {
//     api.close(); // Release PORT 8080
//   });
//   describe("GET /api/movies ", () => {

//     it("should return 20 movies and a status 200", (done) => {

//       request(api)
//         .get("/api/movies")
//         .set("Accept", "application/json")
//         .set('Authorization', 'Bearer ' + authToken)
//         .expect("Content-Type", /json/)
//         .expect(200)
//         .end((err, res) => {
//           expect(res.body.results).to.be.a("array");
//           expect(res.body.results.length).to.equal(20);
//           done();
//         });
//     });
//   });

//   describe("GET /api/movies/:id", () => {
//     describe("when the id is valid", () => {
//       it("should return the matching movie", () => {
//         return request(api)
//           .get(`/api/movies/${movies[0].id}`)
//           .set("Accept", "application/json")
//           .set('Authorization', 'Bearer ' + authToken)
//           .expect("Content-Type", /json/)
//           .expect(200)
//           .then((res) => {
//             expect(res.body).to.have.property("title", movies[0].title);
//           });
//       });
//     });
//     describe("when the id is invalid", () => {
//       it("should return the NOT found message", () => {
//         return request(api)
//           .get("/api/movies/9999")
//           .set("Accept", "application/json")
//           .set('Authorization', 'Bearer ' + authToken)
//           .expect("Content-Type", /json/)
//           .expect(404)
//           .expect({
//             status_code: 404,
//             message: "The resource you requested could not be found.",
//           });
//       });
//     });
//   });
// });
