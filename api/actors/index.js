import express from 'express';
import actorModel from './actorModel';
import asyncHandler from 'express-async-handler';

const router = express.Router()

var verifiedUserToken = false;

async function authTokenMiddleware (req){
try{

const admin = require('firebase-admin')
var serviceAccount = require("../../reactadminservice.json")

if(!admin.apps.length){
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
  });
}else{
    admin.app()
}
const tokenString = req.headers["authorization"] ? req.headers["authorization"].split(" ") : null

console.log(tokenString)
if(!tokenString){
    return false
}
else {
    const {getAuth} = require("firebase-admin/auth")
    getAuth()
    .verifyIdToken(tokenString[1])
    .then((decodedToken)=> {
        const uid = decodedToken.uid;
        console.log(uid);
        verifiedUserToken = true;
        //return true;
    })
    .catch((error) => {
        verifiedUserToken = false;
       //return false;
    })
}}
catch{
    return false;
}
}


router.get('/', asyncHandler(async (req, res) => {
  authTokenMiddleware(req);
  if(verifiedUserToken){
  const actors = await actorModel.find();
  res.status(200).json(actors);
  }
  else{
    res.status(404).json({message: 'The resource you requested could not be found.', status_code: 404});
    }
}));

router.get('/:id', asyncHandler(async (req, res) => {
  authTokenMiddleware(req);
  if(verifiedUserToken){
    const id = parseInt(req.params.id);
    const actor = await actorModel.findByActorDBId(id);
    if (actor) {
        res.status(200).json(actor);
    } else {
        res.status(404).json({message: 'The resource you requested could not be found.', status_code: 404});
    }
  }
  else{
    res.status(404).json({message: 'The resource you requested could not be found.', status_code: 404});
    }
  }));


  router.get('/:id/known_for', asyncHandler(async (req, res) => {
    authTokenMiddleware(req);
    if(verifiedUserToken){
    const id = parseInt(req.params.id);
    const actor = await actorModel.findByActorDBId(id);
    const known_fors = await actorModel.findActorsKnownFor(id);
    console.log(known_fors)
    if (known_fors) {
        res.status(200).json(known_fors);
    } else {
        res.status(404).json({message: 'The resource you requested could not be found.', status_code: 404});
    }
  }
  else{
    res.status(404).json({message: 'The resource you requested could not be found.', status_code: 404});
    }
  }));

export default router;