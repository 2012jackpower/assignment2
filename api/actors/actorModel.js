import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const ActorSchema = new Schema({
  adult: { type: String },
  biography: {type: String },
  birthday: {type: String },
  id: {type: Number, unique: true, required: true },
  imdb_id: {type: String },
  name: {type: String },
  place_of_birth: {type: String },
  profile_path: {type: String },
  known_for: [{
      movie_id: {type: Number}
  }]
});

ActorSchema.statics.findByActorDBId = function (id) {
    return this.findOne({ id: id });
  };

ActorSchema.statics.findActorsKnownFor = function(id) {
    return this.findOne({ id: id },'known_for')
  }

export default mongoose.model('Actors', ActorSchema);