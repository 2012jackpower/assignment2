import mongoose from 'mongoose';

const Schema = mongoose.Schema;


const MovieSchema = new Schema({
  adult: { type: Boolean },
  category: {type: String},
  id: { type: Number, required: true, unique: true },
  poster_path: { type: String },
  overview: { type: String },
  release_date: { type: String },
  original_title: { type: String },
  genre_ids: [{ type: Number }],
  original_language: { type: String },
  title: { type: String },
  backdrop_path: { type: String },
  popularity: { type: Number },
  vote_count: { type: Number },
  video: { type: Boolean },
  vote_average: { type: Number },
  cast : [{
    adult: {type: Boolean},
    gender: { type: Number },
    id: { type: Number },
    known_for_department: { type: String },
    name: { type: String },
    original_name: { type: String },
    popularity: { type: Number },
    profile_path: { type: String },
    cast_id: { type: Number },
    character: { type: String },
    credit_id: { type: String },
    order: { type: Number },
  }],
  backdrops: [{
    aspect_ratio: { type: Number },
    height: { type: Number },
    file_path: { type: String },
    vote_average: { type: Number },
    vote_count: { type: Number },
    width: { type: Number },
  }],
  production_countries: [{
    iso_3166_1: { type: String },
    name: { type: String }
  }],
  runtime: { type: Number },
  spoken_languages: [{
    iso_639_1: { type: String },
    name: { type: String }
  }],
  status: { type: String },
  tagline: { type: String }
});

MovieSchema.statics.findByMovieDBId = function (id) {
  return this.findOne({ id: id });
};
MovieSchema.statics.findMovieImages = function(id) {
  return this.findOne({ id: id },'backdrops')
}
MovieSchema.statics.findKnownForMovies = function(ids) {
  return this.find({ id: { $in: ids}});
}

export default mongoose.model('Movies', MovieSchema);


