import express from 'express';
import {moviesObject} from './moviesData';
import movieModel from './movieModel';
import asyncHandler from 'express-async-handler';

const logger = require('../../Utils/logger')

const router = express.Router(); // eslint-disable-line

var verifiedUserToken = false;

async function authTokenMiddleware (req){
try{

const admin = require('firebase-admin')
var serviceAccount = require("../../reactadminservice.json")

if(!admin.apps.length){
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
  });
}else{
    admin.app()
}
const tokenString = req.headers["authorization"] ? req.headers["authorization"].split(" ") : null

if(!tokenString){
    return false
}
else {
    const {getAuth} = require("firebase-admin/auth")
    getAuth()
    .verifyIdToken(tokenString[1])
    .then((decodedToken)=> {
        const uid = decodedToken.uid;
        console.log(uid);
        verifiedUserToken = true;
        //return true;
    })
    .catch((error) => {
        verifiedUserToken = false;
       //return false;
    })
}}
catch{
    return false;
}
}

router.get('/', asyncHandler(async (req, res) => {
    authTokenMiddleware(req);
    if(verifiedUserToken){
    let { page = 1, limit = 20 } = req.query; // destructure page and limit and set default values
    [page, limit] = [+page, +limit]; //trick to convert to numeric (req.query will contain string values)

    const totalDocumentsPromise = movieModel.estimatedDocumentCount(); //Kick off async calls
    const moviesPromise = movieModel.find().limit(limit).skip((page - 1) * limit);

    const totalDocuments = await totalDocumentsPromise; //wait for the above promises to be fulfilled
    const movies = await moviesPromise;
    const returnObject = { page: page, total_pages: Math.ceil(totalDocuments / limit), total_results: totalDocuments, results: movies };//construct return Object and insert into response object

    res.status(200).json(returnObject);
    }
    else{
    res.status(404).json({message: 'The resource you requested could not be found.', status_code: 404});
    }
}
));

router.get('/known_for/:ids', asyncHandler(async (req, res) => {
authTokenMiddleware(req);
if(verifiedUserToken){
    const KFIds = req.params;
    const idsAsString = KFIds.ids;
    const newArr = idsAsString.split(',');
    const numArray = newArr.map(Number);
    const KFMovies = await movieModel.findKnownForMovies(numArray);
    res.status(200).json(KFMovies);
}
else{
    res.status(404).json({message: 'The resource you requested could not be found.', status_code: 404});
}
}));

//get a movie
router.get('/:id', asyncHandler(async (req, res) => {
authTokenMiddleware(req);
if(verifiedUserToken){
  const id = parseInt(req.params.id);
  const movie = await movieModel.findByMovieDBId(id);
  if (movie) {
      res.status(200).json(movie);
  } else {
      res.status(404).json({message: 'The resource you requested could not be found.', status_code: 404});
  }
}
else{
    res.status(404).json({message: 'The resource you requested could not be found.', status_code: 404});
}
}));

router.get('/:id/images', asyncHandler(async (req, res) => {
authTokenMiddleware(req);
if(verifiedUserToken){
    const id = parseInt(req.params.id);
    const images = await movieModel.findMovieImages(id);
    if (images) {
        res.status(200).json(images);
    } else {
        res.status(404).json({message: 'The resource you requested could not be found.', status_code: 404});
    }
}
else{
    res.status(404).json({message: 'The resource you requested could not be found.', status_code: 404});
}
  }));

export default router;